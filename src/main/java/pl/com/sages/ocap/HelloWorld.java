package pl.com.sages.ocap;

import pl.com.sages.ocap.calc.Calculator;

import java.util.Arrays;

public class HelloWorld {

    public static void main(String[] args) { // psvm
        System.out.println("let's exercise!"); // sout


        String encoding = System.getProperty("file.encoding");
        System.out.println("encoding = " + encoding);

        // Byte, byte: 1 bajt
        // Short, short: 2 bajty
        // Integer, int: 4 bajty
        // Long, long: 8 bajtow

        Integer age1 = Integer.valueOf(127); // autoboxing, immutable, immutability
        //new Integer(18); // min: -128

        Integer age2 = 127;
        //new Integer(18);

        Object s = 127;

        System.out.println("age1==age2 : " + (s==age2));


        // Float, float: 4 bajty
        // Double, double: 8 bajtow
        double value = 23.45;

        System.out.println("value = " + value);


        // Character, char: 2 bajty
        char a = 'a';
        char b = 'b';
        char c = (char)(a + b);
        System.out.println("char c = [" + (short)c +  "]");

        int[] values = /*new int[]*/ {1,2,3,4,5,6,7,8,9,10};
        /*values[0] = 1;
        values[1] = 2;
        values[values.length-1] = 10;*/

        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }

        for ( int i : values ) {
            System.out.println(i);
        }

        System.out.println(Arrays.toString(values));



        int size = args.length>0 ? Integer.parseInt(args[0]) : 10;
        int[][] results = new int[size][size];


        iks:
        for (int x = 0; x < results.length; x++) {
            for(int y = 0; y < results[x].length; y++){
                if(y==13-1){
                    continue iks;
                }
                results[x][y] = Calculator.multiply(x+1, y+1);

            }
        }

        for(int[] row : results){
            for(int cell : row){
                System.out.print(cell + "\t");
            }
            System.out.println();
        }

        System.out.println("Arrays.toString(results) = " + Arrays.toString(results));

        WeekDay weekDay = WeekDay.TUESDAY;

        if(weekDay==WeekDay.MONDAY){
            System.out.println("Monday");
        } else if(weekDay==WeekDay.TUESDAY){
            System.out.println("Tuesday");
        } else if(weekDay==WeekDay.WEDNESDAY){
            System.out.println("Wednesday");
        } else {
            System.out.println("Weekend");
        }

        // primitive, String, enum
        switch (weekDay){
            case FRIDAY: case SATURDAY: case SUNDAY:
                System.out.println("weekend");
                break;
            case MONDAY:
                System.out.println("monday");
                break;
            case TUESDAY:
                System.out.println("tuesday");
                break;
            case WEDNESDAY:
                System.out.println("wednesday");
                break;

        }

        weekDay = WeekDay.values()[1];
        weekDay = WeekDay.TUESDAY;

        System.out.println("day number: " + (weekDay.ordinal()+1) +" is weekend ? " + weekDay.isWeekend());





        int tmp = -128;
        //0b10001010_00000000_00000000_00000000;
        System.out.println("tmp = " + tmp +", " + Integer.toBinaryString(tmp));

        int tmp2 = (tmp >> 2); /// 0b11100010
        System.out.println("tmp2 = " + tmp2 + ", " + Integer.toBinaryString(tmp2));

        int tmp3 = (tmp >>> 2); /// 0b00100010
        System.out.println("tmp3 = " + tmp3 + ", " + Integer.toBinaryString(tmp3));

        boolean b11 = false;
        boolean b12 = true;

        if(method(b11) & method(b12)){
            System.out.println("in");
        } else {
            System.out.println("out");
        }


    }

    private static boolean method(boolean b){
        System.out.println("b=" + b );
        return b;
    }



}
