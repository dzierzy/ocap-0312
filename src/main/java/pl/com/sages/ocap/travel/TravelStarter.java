package pl.com.sages.ocap.travel;

import pl.com.sages.ocap.zoo.Horse;

import java.text.DateFormat;
import java.text.ParseException;

public class TravelStarter {

    public static void main(String[] args) {

        System.out.println("TravelStarter.main");

        String localVariable ="local";
        Transportation t = s -> System.out.println(s);

        String passenger = "Jan Kowalski";
        try {
            t.transport(passenger);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
