package pl.com.sages.ocap.travel;

public class Car /* extends Machine */ implements Transportation{

    public void drive(){
        System.out.println("driving...");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("[String] loading passenger " + passenger);
        drive();
    }

}
