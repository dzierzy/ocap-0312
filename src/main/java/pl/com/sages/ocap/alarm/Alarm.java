package pl.com.sages.ocap.alarm;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Alarm {

    public static void main(String[] args) {

        ScheduledExecutorService es = Executors.newScheduledThreadPool(10);
               // newFixedThreadPool(10);

        Beeper b = new Beeper();
        Light l = new Light();


        es.schedule(b, 20, TimeUnit.SECONDS);
        es.execute(l);

        es.execute( ()-> System.out.println("ad hoc async task"));

     /*   Thread tb = new Thread(b);
        Thread tl = new Thread(l);

        tb.setPriority(10);
        tl.setPriority(5);

        tb.start();
        tl.start();

        try {
            tb.join(); // main czeka na sakonczenie beepera
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        tb.start(); // */

        System.out.println("done.");

        es.shutdown();

    }
}
