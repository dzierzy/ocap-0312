package pl.com.sages.ocap.zoo;

import pl.com.sages.ocap.travel.Transportation;

import java.io.Serializable;

public class Horse extends Animal implements Transportation {


    public Horse(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("running!!!!");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("seating passnger " + passenger);
        move();
    }
}
