package pl.com.sages.ocap.zoo;

import pl.com.sages.ocap.calc.BetterCalculator;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZooStarter {

    private static int y = 7;

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");


        // PECS
        // Producer extends
        // Consumer super

        List<Animal> animals = new ArrayList<>();
        //Set<Animal> animals = new TreeSet<>(new AnimalComparator(true)); // duplicates allowed, ordered

        Pigeon franek = new Pigeon(3, "Franek");
        Horse przewalskiego = new Horse(300, "Przewalskiego");

        animals.add(franek);
        animals.add(przewalskiego);
        animals.add(new Pigeon(5, "Franek"));

        Collections.sort(animals, (a1,a2)->a1.getSize()-a2.getSize() );


        Iterator<Animal> itr = animals.iterator();

        while (itr.hasNext()){
            Animal animal = itr.next();
            System.out.println("animal = " + animal);
        }

        System.out.println("animalList.size() = " + animals.size());


        // 1. supplier
        Supplier<Animal> supplier = () -> new Pigeon(1, "obiadowy");

        Animal animal = supplier.get();
        System.out.println("animal from supplier = " + animal);

        // 2. consumer
        Consumer<Animal> consumer = a -> System.out.println("consuming " + a);
        consumer.accept(animal);
        
        // 3. function (supplier + consumer)
        Function<Animal, Integer> function = a -> a.getSize();
        System.out.println("size = " + function.apply(animal));

        // 4. Predicate
        Predicate<Animal> predicate = a -> a.getSize()>200;
        System.out.println("heavy = " + predicate.test(animal));
        
        
        Stream<Animal> animalStream = animals.stream();
        
        List<String> animalNames =
                animalStream
                .parallel()
                .filter( a -> a.getSize()<200 )
                .sorted( (a1,a2)->a1.getSize()-a2.getSize())
                //.collect(Collectors.toList());
                .peek( a -> System.out.println(a))
                .map(a->a.getName())
                .limit(1)
                .collect(Collectors.toList());


        animalNames.forEach(n-> System.out.println(n));


       IntStream integerStream = IntStream.builder().add(7).add(12).add(2018).build();
       integerStream.forEach(i-> System.out.print(i+"/"));

    }
}
