package pl.com.sages.ocap.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {


    boolean ascending;

    public AnimalComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(Animal o1, Animal o2) {
        return ascending ? o1.getSize()-o2.getSize() : o2.getSize()-o1.getSize();
    }
}
