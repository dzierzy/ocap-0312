package pl.com.sages.ocap.zoo;

public class Pigeon extends Animal {

    public Pigeon(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("flying...");
    }
}
