package pl.com.sages.ocap.zoo;

import java.util.Objects;

public abstract class Animal implements Comparable<Animal> {

    private int size;

    private String name;

    public Animal(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public void eat(String food){
        System.out.println("eating " + food);
    }

    public abstract void move();

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    /*
        equals & hashCode contract

        int h1 = o1.hashCode();
        if(h1==h2){
            b = o1.equals(o2);
        } else {
            b = false;
        }

    */

    @Override
    public boolean equals(Object o) {
        System.out.println("in equals " + this);
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (getSize() != animal.getSize()) return false;
        return getName() != null ? getName().equals(animal.getName()) : animal.getName() == null;
    }

    @Override
    public int hashCode() {
        int h = Objects.hash(size, name);
        System.out.println("in hashCOde " + h + ", " + this);
        return h;
    }

    @Override
    public int compareTo(Animal o) {
        return o.getSize()-this.getSize();
    }

    @Override
    public String toString() {
        return "Animal{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }
}
