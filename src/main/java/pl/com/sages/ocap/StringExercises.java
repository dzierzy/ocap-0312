package pl.com.sages.ocap;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringExercises {

    public static void main(String[] args) {
        System.out.println("StringExercises.main");

        String[] strings = {"abc", "def", "ghi", "jkl"};


        StringBuilder sb = new StringBuilder();
        for(String s : strings ){
            //tmp += s;
            sb.append(s);

        }
        System.out.println("tmp = " + sb.toString());

        String s1 = "Kowalski";
        String s2 = new String("Kowalski");
        String s3 = "Kowalski";

        String s4 = new String(new char[]{'K','o','w','a','l','s','k','i'});


        System.out.println("s1==s2 ? " + (s1.equals(s2)));
        System.out.println("s1==s4 ? " + (s1.equals(s4)));
        System.out.println("s2==s3 ? " + (s2.equals(s3)));
        //System.out.println("s1==args[0] ? " + (s1==args[0]));

        System.out.println("zażółć gęślą jaźń".toUpperCase()
                .equalsIgnoreCase("zażółć gęślą jaźń"));
        // \u0123

        String diactrics = "zażółć gęślą jaźń";

        String[] words = diactrics.split(" ");

        System.out.println("Arrays.toString(words) = " + Arrays.toString(words));


        StringTokenizer tokenizer = new StringTokenizer(diactrics);
        while(tokenizer.hasMoreTokens()){
            String word = tokenizer.nextToken();
            System.out.println("word = " + word);
            if(word.equals("gęślą")) break;
        }

        String text = "defabcd5fghijkl";
        Pattern p = Pattern.compile("d\\wf");
        Matcher m = p.matcher(text);
        while(m.find()){
            System.out.println("start:" + m.start()
                    + ", end:" + m.end()
                    + ", group:" + m.group());
        }
    }
}
