package pl.com.sages.ocap.charger;


public class ChargingStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadPrintStream());

        //Electricity.getInstance().turnOn();

        Charger samsungCharger = new Charger();
        Charger iphoneCharger = new Charger();

        Phone iphone = new Phone(0, "iphone8");
        Phone samsung = new Phone(75, "samsung s8");

        iphoneCharger.chargeDevice(iphone, 3000);
        samsungCharger.chargeDevice(iphone, 2000);


        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Electricity.getInstance().turnOn();

        System.out.println("done.");

        Charger.es.shutdown();

    }



}
