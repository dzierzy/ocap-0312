package pl.com.sages.ocap.charger;

// Singleton
public class Electricity {

    private boolean on = false;

    private static Electricity instance;

    private Electricity(){
    }

    public synchronized static Electricity getInstance(){
        if(instance==null){
            instance = new Electricity();
        }
        return instance;
    }

    public void turnOn(){
        on = true;
        synchronized (this){
            this.notifyAll();
        }
    }

    public void turnOff(){
        on = false;
    }

    public boolean isOn() {
        return on;
    }
}
