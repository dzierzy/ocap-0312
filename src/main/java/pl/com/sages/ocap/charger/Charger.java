package pl.com.sages.ocap.charger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Charger implements Runnable{


    static ExecutorService es = Executors.newScheduledThreadPool(5);

    private static final int POWER = 2;

    private int hours;

    private Phone p;
    

    public void chargeDevice(Phone p, int hours) {
        this.hours = hours;
        this.p = p;

        es.execute(this);
    }

    @Override
    public void run() {
        p.charge(hours * POWER);
    }
}
