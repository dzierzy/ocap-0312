package pl.com.sages.ocap.charger;

import java.util.concurrent.atomic.AtomicInteger;

public class Phone {

    private AtomicInteger battery;

    private String name;

    public Phone(int battery, String name) {
        this.battery = new AtomicInteger(battery);
        this.name = name;
    }

    public /*synchronized*/ void charge(int circles) {


        for (int i = 1; i <= circles; i++) {
           //System.out.println("charging " + this + "...");

            if(!Electricity.getInstance().isOn()){
                System.out.println("no electricity, waiting....");
                try {
                    synchronized (Electricity.getInstance()) {
                        Electricity.getInstance().wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("electricity is back!");
            }

           battery.incrementAndGet();
           /*synchronized ( this ) {
               battery++; // battery = battery + 1;
           }*/
        }

        System.out.println("charging finished " + this + ".");

    }


    @Override
    public String toString() {
        return "Phone{" +
                "battery=" + battery +
                ", name='" + name + '\'' +
                '}';
    }
}
