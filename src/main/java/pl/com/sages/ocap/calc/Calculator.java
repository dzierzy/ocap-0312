package pl.com.sages.ocap.calc;

import pl.com.sages.ocap.calc.memory.ListMemory;
import pl.com.sages.ocap.calc.memory.Memory;
import pl.com.sages.ocap.calc.memory.SimpleMemory;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Calculator /*extends Object*/ implements Serializable {

    private static final long serialVersionUID = -1L;

    protected Memory memory;

    {
        System.out.println("initialization block");

    }

    public Calculator(double value) {
        initMemory();
        memory.writeValue(value);
        System.out.println("parametrized constructor: " + memory.readValue());
    }

    public Calculator() {
        this(0.0);
        System.out.println("default constructor: " + memory.readValue());
    }

    private void initMemory() {

       /* File dir = new File(
                "c:" + File.separator + "tmp" + File.separator +".." + File.separator +
                "tmp" + File.separator + "calc");
*/
        Path dirPath = Paths.get("c:", "tmp", "calc");

        try {

            if(!Files.exists(dirPath)){
                Files.createDirectories(dirPath);
            }

            /*if (!dir.exists()) {
                System.out.println("creating " + dir.getAbsolutePath() + ", canonical: " + dir.getCanonicalPath());
                dir.mkdirs();
            }*/

            Path filePath = Paths.get(dirPath.toString(), "memory.config");
            //File file = new File(dir, "memory.config");

            String memoryClassName = "pl.com.sages.ocap.calc.memory.SimpleMemory";
            //if(file.exists()){
            if(Files.exists(filePath)){
                // java 7: try-with-resources
                try(BufferedReader br = new BufferedReader(new FileReader(filePath.toFile()));){

                    memoryClassName = br.readLine();
                    System.out.println("memoryClassName = " + memoryClassName);

                } /*finally {
                    if(br!=null){
                        try{
                            br.close();
                        }catch(IOException ioe){}
                    }
                }*/
            } else {
                System.out.println("memory config missing, working with default impl");
                //file.createNewFile();
                Files.createFile(filePath);

                try(BufferedWriter bw = new BufferedWriter(new FileWriter(filePath.toFile(), true)) ) {
                    bw.write(memoryClassName);
                }
            }

            Class clazz = Class.forName(memoryClassName);
            Constructor memoryDefaultConstructor = clazz.getConstructor();
            Object o = memoryDefaultConstructor.newInstance();
            if(o instanceof Memory){
                memory = (Memory)o;
                System.out.println("memory created: " + memory.getClass().getName());
                return;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        memory = new SimpleMemory();
    }

    public double add(double operand) { // varargs
        memory.writeValue(memory.readValue() + operand);
        return memory.readValue();
    }

    public double subtract(double operand) {
        memory.writeValue(memory.readValue() - operand);
        return memory.readValue();
    }


    public double multiply(double operand) {
        memory.writeValue(memory.readValue() * operand);
        return memory.readValue();
    }

    public double divide(double operand) {
        memory.writeValue(memory.readValue() / operand);
        return memory.readValue();
    }


    public static int multiply(int iks, int igrek) {
        System.out.println("static method in C");
        return iks * igrek;
    }

    public double getValue() {
        return memory.readValue();
    }


    @Override
    public String toString() {
        return new String("Calculator{")
                .concat("value=")
                .concat(memory.toString())
                .concat("}");

    }
}
