package pl.com.sages.ocap.calc;

import pl.com.sages.ocap.calc.memory.ListMemory;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Date;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class CalcStarter {

    public static void main(String[] args) {
        System.out.println("CalcStarter.main");
        try {

            Supplier<BetterCalculator> calcSupplier = () -> new BetterCalculator(0);
                    //BetterCalculator::new;

            BetterCalculator bc = calcSupplier.get();
            
            BiFunction<Integer, Integer, Integer> multiplyFunction = Calculator::multiply;

            int multiplyResult = multiplyFunction.apply(2,4);
            System.out.println("r = " + multiplyResult);
            
            Function<Double, Double> subtractFunction = bc::subtract; //(d, e) -> bc.subtract(d, e);
            subtractFunction.apply(5.0);

            bc.multiply(2, 3);
            bc.add(2);

            System.out.println("bc.getValue() = " + bc.getValue());

            /*File f = new File("calc.serialized");
            Calculator c = null;
            try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
                c = (Calculator)ois.readObject();
            }catch (Exception e){
                e.printStackTrace();
                c = new BetterCalculator();
            }*/

            double initValue = 0;
            try(Connection connection = getConnection();){
                System.out.println("connected.");
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery("select value from memory order by id desc limit 1");
                if (rs.next()){
                    initValue = rs.getDouble("value");
                }
            }
            System.out.println("disconnected. initValue=" + initValue);



            Calculator c = new BetterCalculator(initValue);

            c.multiply(3.5);
            c.subtract(2.75);
            c.divide(2);
            c.add(1);


            /*try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))){
                oos.writeObject(c);
            }*/

            try(Connection connection = getConnection()){

                connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                connection.setAutoCommit(false);
                try {
                    PreparedStatement stmt = connection.prepareStatement("insert into memory (value) values (?)");

                    if (c.memory instanceof ListMemory) {
                        ListMemory lm = (ListMemory) c.memory;
                        for (double v : lm.getValues()) {
                            stmt.setDouble(1, v);
                            //stmt.executeUpdate();
                            stmt.addBatch();
                        }

                        stmt.executeBatch();
                        connection.commit();
                    }
                }catch (Exception e){
                    connection.rollback();
                    throw e;
                }

            }







            Locale locale = new Locale("en", "GB");
            NumberFormat nf = new DecimalFormat("###,###.####");
            nf.setMaximumFractionDigits(6);
            nf.setMinimumFractionDigits(6);
            nf.setMinimumIntegerDigits(2);

            Date date = new Date(); // EPOC -> 1/1/1970 00:00:00 in millis
            Calendar calendar = Calendar.getInstance();
            calendar.set(2018, 11, 24);
            calendar.add(Calendar.DATE, 7);
            calendar.add(Calendar.HOUR, 2);
            date = calendar.getTime();

            DateFormat df = new SimpleDateFormat("yyyy>MM>dd hh>mm>ss>SS G ww WW");

            LocalDateTime ld = LocalDateTime.now(); // LocalDate, LocalTime
            //ZonedDateTime zdt = ZonedDateTime.now();
            //Instant i = Instant.now();

            Duration duration = Duration.between(ld, LocalDateTime.of(2018, 12, 31, 16, 30));
            //Period p = Period.between(ld.toLocalDate(), LocalDate.of(2018, 12, 31));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh>mm>ss>SS");
            String timestamp = ld.format(formatter);
            System.out.println("[" + timestamp + "] c.value=" + nf.format(c.getValue()));

            System.out.println("duration in days: " + duration.toDays());

            Locale locale2 = new Locale("pl", "PL");
            ResourceBundle rb = ResourceBundle.getBundle("sample", locale2);

            String sampleText = rb.getString("sample.text");
            System.out.println("sampleText = " + sampleText);

        } catch (Throwable e) {
            System.out.println("unhandled exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static Connection getConnection(){

        String driverName = "com.mysql.cj.jdbc.Driver";
        String jdbcUrl = "jdbc:mysql://localhost/calculator?serverTimezone=UTC";
        String user = "root";
        String password = "mysql";

        try {
            Class.forName(driverName);
            Connection c = DriverManager.getConnection(jdbcUrl, user, password);
            c.setAutoCommit(false);
            return c;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }
}
