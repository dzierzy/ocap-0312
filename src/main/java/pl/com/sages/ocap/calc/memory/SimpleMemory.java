package pl.com.sages.ocap.calc.memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public double readValue() {
        return value;
    }

    @Override
    public void writeValue(double value) {
        this.value = value;
    }

    @Override
    public int capacity() {
        return 1;
    }
}
