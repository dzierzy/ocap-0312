package pl.com.sages.ocap.calc.memory;

public interface Memory {

    double readValue();

    void writeValue(double value);

    int capacity();

}
