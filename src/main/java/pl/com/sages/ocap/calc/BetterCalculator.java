package pl.com.sages.ocap.calc;

import java.io.IOException;
import java.io.Serializable;

public class BetterCalculator extends Calculator implements Serializable {

    public BetterCalculator(double initValue){
        super(initValue);
        System.out.println("BC constructor");
    }

    public double pow(int power) throws CalculatorException {
        memory.writeValue(Math.pow(memory.readValue(),power));
        return memory.readValue();
    }

    @Override
    public double divide(double operand) {
        if(operand==0.0){
            throw new IllegalArgumentException("pamietaj cholero nie dziel przez zero");
        }
        return super.divide(operand);
    }

}
