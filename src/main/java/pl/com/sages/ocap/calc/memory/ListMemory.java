package pl.com.sages.ocap.calc.memory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory, Serializable {

    private List<Double> values = new ArrayList<>();

    @Override
    public double readValue() {
        return values.get(values.size()-1);
    }

    @Override
    public void writeValue(double value) {
        values.add(value);
    }

    @Override
    public int capacity() {
        return Integer.MAX_VALUE;
    }

    public List<Double> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "ListMemory{" +
                "values=" + values +
                '}';
    }
}
