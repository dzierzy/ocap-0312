package pl.com.sages.ocap;

public enum WeekDay {

    MONDAY(false), // = new WeekDay();
    TUESDAY(false),
    WEDNESDAY,
    THURSDAY,
    FRIDAY(true),
    SATURDAY(true),
    SUNDAY(true);

    private final boolean weekend;

    WeekDay(boolean weekend){
        this.weekend = weekend;
    }

    WeekDay(){
        this(false);
    }

    public boolean isWeekend() {
        return weekend;
    }

}
